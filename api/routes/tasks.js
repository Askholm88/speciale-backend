const express = require("express");
const router = express.Router();
const { Pool } = require("pg");

const pool = new Pool({
  user: process.env.RDS_USERNAME,
  host: "tasksdb.czoxykybqree.us-east-2.rds.amazonaws.com",
  database: "postgres",
  password: process.env.RDS_PW,
  port: 5432
})

router.get("/", (req, res, next) => {
  pool.connect((err, client, release) => {
    if (err) {
      return console.error("Error acquiring client", err.stack);
    }
    client.query("SELECT * FROM tasks ORDER BY id", (err, result) => {
      release();
      if (err) {
        return console.error("Error executing query", err.stack);
      }
      console.log(result.rows);
      if (result.rows.length > 0) {
        res.status(200).json({
          message: "Returned all entries from tasksdb",
          numberOfEntries: result.rows.length,
          fetchedTasks: result.rows
        });
      } else {
        res.status(404).json({
          message: "Didn't find any entries"
        });
      }
    });
  });
});

router.post("/", (req, res, next) => {
  pool.connect((err, client, release) => {
    if (err) {
      return console.error("Error acquiring client", err.stack);
    }
    client.query(
      "INSERT INTO tasks(id, description, code, answer) VALUES($1, $2, $3, $4)",
      [req.body.id, req.body.description, req.body.code, req.body.answer],
      (err, result) => {
        release();
        if (err) {
          res.status(500).json({
            error: err
          });
          return console.error("Error executing query", err.stack);
        }
        res.status(201).json({
          message: "Handling POST request for tasks",
          createdTask: req.body
        });
        console.log(req.body);
      }
    );
  });
});

router.get("/:taskId", (req, res, next) => {
  const id = req.params.taskId;
  pool.connect((err, client, release) => {
    if (err) {
      return console.error("Error acquiring client", err.stack);
    }
    client.query("SELECT * FROM tasks WHERE id = $1", [id], (err, result) => {
      release();
      if (err) {
        return console.error("Error executing query", err.stack);
      }
      console.log(result.rows);
      if (result.rows.length > 0) {
        res.status(200).json({
          message: "Handling GET request for tasks",
          fetchedTask: result.rows
        });
      } else {
        res.status(404).json({
          message: "Didn't find any task with id"
        });
      }
    });
  });
});

router.patch("/:taskId", (req, res, next) => {
  const id = req.params.taskId;
  pool.connect((err, client, release) => {
    if (err) {
      return console.error("Error acquiring client", err.stack);
    }
    client.query(
      "UPDATE tasks SET description = $1, code = $2, answer = $3 WHERE id = $4",
      [req.body.description, req.body.code, req.body.answer, id],
      (err, result) => {
        release();
        if (err) {
          return console.error("Error executing query", err.stack);
        }
        console.log(result.command);
        res.status(200).json({
          message: "Update task entry with data",
          command: result.command
        });
      }
    );
  });
});

router.delete("/:taskId", (req, res, next) => {
  pool.connect((err, client, release) => {
    if (err) {
      return console.error("Error acquiring client", err.stack);
    }
    client.query(
      "DELETE FROM tasks WHERE id = $1",
      [req.body.id],
      (err, result) => {
        release();
        if (err) {
          res.status(500).json({
            error: err
          });
          return console.error("Error executing query", err.stack);
        }
        res.status(200).json({
          message: "Deleted task from tasksdb",
          deletedTaskId: req.body.id
        });
        console.log(result);
      }
    );
  });
});

module.exports = router;
