const express = require('express')
const router = express.Router()

router.get('/', (req,res, next) => {
    res.status(200).json({
        message: 'Answers were fetched'
    })
})

router.post('/', (req,res, next) => {
    const answer = {
        answerId: req.body.answerId,
        answer: req.body.answer
    }
    res.status(201).json({
        message: 'Answers were created',
        answer: answer
    })
})

router.get('/:answerId', (req,res, next) => {
    res.status(200).json({
        message: 'Answer details',
        answerId: req.params.answerId
    })
})

router.delete('/:answerId', (req,res, next) => {
    res.status(200).json({
        message: 'Answer details',
        answerId: req.params.answerId
    })
})

module.exports = router