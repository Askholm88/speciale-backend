const express = require("express");
const router = express.Router();
const { Pool } = require("pg");

const pool = new Pool({
    user: process.env.RDS_USERNAME,
    host: "tasksdb.czoxykybqree.us-east-2.rds.amazonaws.com",
    database: "postgres",
    password: process.env.RDS_PW,
    port: 5432
  });

  router.get("/", (req, res, next) => {
      pool.connect((err, client, release) => {
          if (err) {
              return console.error("Error acquiring client", err.stack)
          }
          client.query(
              "SELECT username, added_score, achievements, scores FROM users ORDER BY added_score DESC",
              (err, result) => {
                  release()
                  if (err) {
                      res.status(404).json({
                          message: "Scores not found"
                      })
                      return console.error("Error executing query", err.stack)
                  } else {
                      return res.status(200).json({
                          message: "Fetched scores",
                          scores: result.rows
                      })
                  }
              }
          )
      })
  }) 

  router.get('/:username', (req, res, next) => {
      pool.connect((err, client, release) => {
          if (err) {
              return console.error('Error acquiring client', err.stack)
          }
          client.query("SELECT * from users WHERE username=$1",
          [req.params.username],
          (err, result) => {
            release();
            if (err) {
              res.status(409).json({
                message: "Couldnt find score"
              });
              return console.error("Error executing query", err.stack);
            } else {
              return res.status(200).json({
                message: "Score fetched from " + req.params.username,
                scores: result.rows[0].scores
              });
            }
          }
          )
      })
  })

router.get("/", (req, res, next) => {
  pool.connect((err, client, release) => {
    if (err) {
      return console.error("Error acquiring client", err.stack);
    }
    client.query(
      "SELECT username, added_score, scores FROM users ORDER BY added_score DESC",
      (err, result) => {
        release();
        if (err) {
          res.status(404).json({
            message: "Scores not found",
          });
          return console.error("Error executing query", err.stack);
        } else {
          return res.status(200).json({
            message: "Fetched scores",
            scores: result.rows,
          });
        }
      }
    );
  });
});

router.get("/:username", (req, res, next) => {
  pool.connect((err, client, release) => {
    if (err) {
      return console.error("Error acquiring client", err.stack);
    }
    client.query(
      "SELECT * from users WHERE username=$1",
      [req.params.username],
      (err, result) => {
        release();
        if (err) {
          res.status(409).json({
            message: "Couldnt find score",
          });
          return console.error("Error executing query", err.stack);
        } else {
          return res.status(200).json({
            message: "Score fetched from " + req.params.username,
            scores: result.rows[0].scores,
          });
        }
      }
    );
  });
});

router.post("/:username", (req, res, next) => {
  pool.connect((err, client, release) => {
    if (err) {
      return console.error("Error acquiring client", err.stack);
    }
      client.query("UPDATE users SET scores=$1, added_score=$2 WHERE username=$3",
      [req.body.taskPoints, req.body.addedScore , req.body.username],
      (err, result) => {
        release();
        if (err) {
          res.status(409).json({
            message: "Couldnt find score",
          });
          return console.error("Error executing query", err.stack);
        } else {
          return res.status(200).json({
            message: "Score posted with " + req.params.username,
            scores: req.params.taskPoints,
          });
        }
      }
    );
  });
});

module.exports = router;
