const express = require("express");
const router = express.Router();
const { Pool } = require("pg");
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken')

const User = require("../models/user");

const pool = new Pool({
  user: process.env.RDS_USERNAME,
  host: "tasksdb.czoxykybqree.us-east-2.rds.amazonaws.com",
  database: "postgres",
  password: process.env.RDS_PW,
  port: 5432
});

router.post("/signup", (req, res, next) => {
  bcrypt.hash(req.body.password, 10, (err, hash) => {
    if (err) {
      return res.status(500).json({
        error: err
      });
    } else {
      const user = new User(req.body.name, hash);
      pool.connect((err, client, release) => {
        if (err) {
          return console.error("Error acquiring client", err.stack);
        }
        client.query(
          "INSERT INTO users(username, password, created_on, added_score, achievements, scores) VALUES ($1, $2, NOW(), $3, $4, $5)",
          [user.name, user.password, 0, { "1": "0", "2": "0", "3": "0", "4": "0", "5": "0"}, {"task1":"0", "task2": "0", "task3": "0", "task4": "0"}],
          (err, result) => {
            release();
            if (err) {
              return res.status(409).json({
                message: "Username is already taken"
              });
              return console.error("Error executing query", err.stack);
            } else {
              console.log(user);
              return res.status(201).json({
                message: "User created successfully"
              });
            }
          }
        );
      });
    }
  });
});

router.post("/login", (req, res, next) => {
  pool.connect((err, client, release) => {
    if (err) {
      return console.error("Error acquiring client", err.stack);
    }
    client.query(
      "SELECT * FROM users WHERE username=$1",
      [req.body.name],
      (err, result) => {
          const user = result.rows[0]
        release();
        if (err) {
          res.status(401).json({
            message: "Auth failed"
          });
          return console.error("Error executing query", err.stack);
        } else if (result.rows[0]) {
            bcrypt.compare(req.body.password, result.rows[0].password, (err, result) => {
                if (err) {
                    res.status(401).json({
                        message: "Auth failed"
                      })}
                if (result) {
                    const token = jwt.sign({
                        username: user.username,
                        userId: user.user_id
                    }, process.env.JWT_KEY, {
                        expiresIn: "1h"
                    })
                    return res.status(200).json({
                        message: 'Auth succesful',
                        token: token,
                        addedScore: user.added_score,
                        achievements: user.achievements,
                        taskScore: user.scores,
                    })
                } else {
                    res.status(401).json({
                        message: "Auth failed"
                    })
                }
            })
        } else {
            res.status(500).json({
                message: "Auth failed"
            })
        }
      }
    );
  });
});

router.delete("/:userID", (req, res, next) => {
  pool.connect((err, client, release) => {
    if (err) {
      return console.error("Error acquiring client", err.stack);
    }
    client.query(
      "DELETE FROM users WHERE username=$1",
      [req.params.userID],
      (err, result) => {
        release();
        if (err) {
          res.status(409).json({
            message: "Username is already taken"
          });
          return console.error("Error executing query", err.stack);
        } else {
          return res.status(201).json({
            message: "Deleted user with username " + req.params.userID
          });
        }
      }
    );
  });
});

module.exports = router;
