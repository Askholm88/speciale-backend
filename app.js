const express = require('express')
const app = express()
const morgan = require('morgan')
const bodyParser = require('body-parser')
const { Pool } = require('pg')
const cors = require('cors')

const taskRoutes = require('./api/routes/tasks')
const answerRoutes = require('./api/routes/answers')
const userRoutes = require('./api/routes/user')
const scoreRoutes = require('./api/routes/scores')

const pool = new Pool({
  user: process.env.RDS_USERNAME,
  host: 'tasksdb.czoxykybqree.us-east-2.rds.amazonaws.com',
  database: 'tasksdb',
  password: process.env.RDS_PW,
  port: 5432,
})

app.use(cors())
app.use(morgan('dev'))
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.options('/', (req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "*");
  if (req.method === "OPTIONS") {
      res.header("Access-Control-Allow-Methods", "POST, GET, SET, DELETE, PATCH, HEAD, PUT")
      return res.status(200).json({
        message: 'something is wrong'
      })
  }
  next()
})

app.use('/tasks', taskRoutes)
app.use('/answers', answerRoutes)
app.use('/user', userRoutes)
app.use('/scores', scoreRoutes)


app.use((req, res, next) => {
  const error = new Error('Not found!')
  error.status = 404
  next(error)
})

app.use((error, req, res, next) => {
  res.status(error.status ||500)
  res.json({
    error: {
      message: error.message
    }
  })
})


/* 

const getTask = () => {
  pool.query(`SELECT * FROM tasks WHERE id = 1`, (err, res) => {
    console.log(err, res)
    pool.end()
  })
}
 */

module.exports = app


/* pool.query(`SELECT * FROM tasks`, (err, res) => {
  console.log(err, res)
  pool.end()
}) */

/* pool.query(`SELECT * FROM tasks`, (err, res) => {
    console.log(err, res)
    pool.end()
}) */




/* const client = new Client({
    connectionString: connection,
  })
  client.connect()
  
  client.query('SELECT NOW()', (err, res) => {
    console.log(err, res)
    client.end()
  })

pool.query('SELECT NOW()', (err, res) => {
  console.log(err, res)
  pool.end()
}) */
